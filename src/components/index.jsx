import ChoiceCard from "./Games/ChoiceCard/ChoiceCard";
import Navigation from "./Navigation";
import Title from "./Games/Title/Title";
import VersusWrapper from "./Games/VersusWrapper/VersusWrapper";
import RefreshButton from "./Games/RefreshButton/RefreshButton";
import Result from "./Games/Result/Result";
import RPSLogo from "./Games/RPSLogo/RPSLogo";
import Back from "./Games/Back/Back";

export {
  ChoiceCard,
  Navigation,
  Title,
  VersusWrapper,
  RefreshButton,
  Result,
  RPSLogo,
  Back,
};
