import React, { Component } from "react";
import { backIcon } from "../../../assets/image";
import "./Back.css";

class Back extends Component {
  render() {
    const { onClick } = this.props;
    return (
      <img onClick={onClick} src={backIcon} alt="back" className="back-icon" />
    );
  }
}

export default Back;
