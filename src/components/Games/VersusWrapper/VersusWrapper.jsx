import React, { Component } from "react";
import "./VersusWrapper.css";

class VersusWrapper extends Component {
  render() {
    return (
      <div className="versus-wrapper d-block" id="versus-wrapper">
        <h1 className="text-uppercase text-center font-weight-bold versus-text">
          vs
        </h1>
      </div>
    );
  }
}

export default VersusWrapper;
