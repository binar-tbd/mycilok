import React, { Component, Fragment } from "react";
import "./Result.css";

class Result extends Component {
  render() {
    const { result, label } = this.props;
    return (
      <div className="result-body" id={`${result}-result`}>
        {result === "draw" && (
          <h2 className="text-uppercase p-0 m-0 draw-text">{label}</h2>
        )}
        {(result === "player-1" || result === "com") && (
          <Fragment>
            <h2 className="text-uppercase p-0 m-0 player-text">{label}</h2>
            <h2 className="text-uppercase p-0 m-0 player-text">win</h2>
          </Fragment>
        )}
      </div>
    );
  }
}

export default Result;
