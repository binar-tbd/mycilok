import React, { Component } from "react";
import "./ChoiceCard.css";

class ChoiceCard extends Component {
  render() {
    const { image, name, type, onClick } = this.props;

    return (
      <div className={type} id={name} onClick={onClick}>
        <img src={image} alt={name} className={name} id={name} />
      </div>
    );
  }
}

export default ChoiceCard;
