import React, { Component } from "react";
import { RPSImage } from "../../../assets/image";
import "./RPSLogo.css";

class RPSLogo extends Component {
  render() {
    return <img src={RPSImage} alt="RPS logo" className="logo" />;
  }
}

export default RPSLogo;
