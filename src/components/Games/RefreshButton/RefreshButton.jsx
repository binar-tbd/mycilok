import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { refreshImage } from "../../../assets/image";
import "./RefreshButton.css";

class RefreshButton extends Component {
  render() {
    const { onClick } = this.props;
    return (
      <Button className="btn-refresh" onClick={onClick}>
        <img src={refreshImage} alt="refresh" className="refresh" />
      </Button>
    );
  }
}

export default RefreshButton;
