import React, { Component } from "react";
import "./Title.css";

class Title extends Component {
  render() {
    const { customClass, label } = this.props;
    return (
      <h1
        className={`${
          customClass ? customClass : null
        } text-uppercase font-weight-bold`}
      >
        {label}
      </h1>
    );
  }
}

export default Title;
