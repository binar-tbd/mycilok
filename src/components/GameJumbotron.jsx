import React, { Component } from "react";
import { Container } from "reactstrap";
import "../assets/css/myJumbotron.css";

class GameJumbotron extends Component {
  state = {};
  render() {
    return (
      <Container className="myLanding mt-5">
        <div className="container-fluid shadow-lg bg-info mx-auto px-5 py-5 text-dark rounded-3">
          <p className="display-4 shadow px-1 py-2">Ciloks GameZone</p>
          <p className="lead">
            This is a simple hero unit, a simple Jumbotron-style component for
            calling extra attention to featured content or information.
          </p>
          <hr className="my-2 text-white" />
          <p className="text-white">
            It uses utility classes for typography and spacing to space content
            out within the larger container.
          </p>
          <p className="lead">
            <a className="btn btn-warning shadow-lg" href="#two">
              Choose Game
            </a>
          </p>
        </div>
      </Container>
    );
  }
}

export default GameJumbotron;
