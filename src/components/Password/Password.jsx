import React, { Component } from "react";

class Password extends Component {
  render() {
    const { value, placeholder, name } = this.props;
    return (
      <input
        type="password"
        value={value}
        placeholder={placeholder}
        name={name}
        className="form-control mb-1"
      />
    );
  }
}

export default Password;
