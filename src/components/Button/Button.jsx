import React, { Component, Fragment } from "react";
import { Container } from "reactstrap";
import "../Button/Button.css";

class Button extends Component {
  state = {
    loading: false,
  };
  fetchData = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 2000);
  };

  render() {
    const { loading } = this.state;
    return (
      <Container className="p-4">
        <button className="button" onClick={this.fetchData}>
          {loading}
          {loading && <span>Loading...</span>}
          {!loading && <span>Login</span>}
        </button>
      </Container>
    );
  }
}
export default Button;
