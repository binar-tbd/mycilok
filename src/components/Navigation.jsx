import React, { Component } from "react";
import "../assets/css/navigation.css";
import { favIcon as cilokIcon } from "../assets/image";
import { Container, Nav, Navbar } from "react-bootstrap";
import firebase from "../services/firebase";
import { AuthContext } from "../auth/auth";

class Navigation extends Component {
  static contextType = AuthContext;
  state = { isOpen: false };
  toggleCollapse = () => {};

  handleLogout = () => {
    const answer = window.confirm("Anda yakin ingin logout?");
    if (answer) {
      firebase.auth().signOut();
      alert("Anda telah logout!");
    } else {
      alert("Please enjoy our game!");
    }
  };

  render() {
    const { currentUser } = this.context;
    // console.log(currentUser);
    return (
      <Navbar
        bg="dark"
        variant="dark"
        expand="lg"
        sticky="top"
        className="navbar py-0 shadow-lg"
      >
        <Container>
          <Navbar.Brand href="/">
            <img src={cilokIcon} className="logo" alt="MyCilok" /> MyCilok
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav
              as="ul"
              className="ms-lg-5 justify-content-between"
              style={{ width: "15%" }}
            >
              <Nav.Item as="li">
                <Nav.Link href="/">Home</Nav.Link>
              </Nav.Item>
              <Nav.Item as="li">
                <Nav.Link href="/game">Game</Nav.Link>
              </Nav.Item>
            </Nav>
            {currentUser ? (
              <Nav
                as="ul"
                className="ms-lg-auto justify-content-between"
                style={{ width: "20%" }}
              >
                <Nav.Item as="li">
                  <Nav.Link href="/profile">Profile</Nav.Link>
                </Nav.Item>
                <Nav.Item as="li">
                  <Nav.Link href="/" onClick={this.handleLogout}>
                    Logout
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            ) : (
              <Nav
                as="ul"
                className="ms-lg-auto justify-content-between"
                style={{ width: "20%" }}
              >
                <Nav.Item as="li">
                  <Nav.Link href="/register">Register</Nav.Link>
                </Nav.Item>
                <Nav.Item as="li">
                  <Nav.Link href="/login">Login</Nav.Link>
                </Nav.Item>
              </Nav>
            )}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default Navigation;
