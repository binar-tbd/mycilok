import React, { Component, Fragment } from "react";
import Navigation from "../components/Navigation";
import GameCard from "../components/GameCard";
import { bgImage } from "../assets/image";

class Game extends Component {
  state = {
    games: [],
  };
  componentDidMount() {
    fetch(`http://localhost:5000/api/v1/games`)
      .then((response) => response.json())
      .then((result) => {
        this.setState({ games: result.data });
      });
  }
  render() {
    return (
      <Fragment>
        <Navigation />
        <div
          className="row row mx-auto p-5"
          style={{
            backgroundImage: `url(${bgImage})`,
            backgroundSize: "cover",
            height: "100%",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundAttachment: "fixed",
          }}
        >
          {this.state.games.map((data) => {
            return (
              <GameCard
                key={data.uuid}
                img={data.image}
                title={data.name}
                uuid={data.uuid}
                detail={data.detail}
              />
            );
          })}
        </div>
      </Fragment>
    );
  }
}

export default Game;
