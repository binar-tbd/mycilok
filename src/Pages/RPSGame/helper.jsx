const getComHand = () => {
  const arrayHand = ["rock", "paper", "scissors"];
  const hand = arrayHand[Math.floor(Math.random() * arrayHand.length)];
  // document
  //   .querySelector(`.com-choice-box#${hand}`)
  //   .classList.add("active-hand");

  setActiveHand("com", hand);

  return hand;
};

const getResult = (playerHand, comHand) => {
  let result = "";

  if (playerHand === comHand) {
    result = "draw";
  }

  if (playerHand === "rock") {
    if (comHand === "paper") {
      result = "com";
    }

    if (comHand === "scissors") {
      result = "player1";
    }
  }

  if (playerHand === "paper") {
    if (comHand === "rock") {
      result = "player1";
    }

    if (comHand === "scissors") {
      result = "com";
    }
  }

  if (playerHand === "scissors") {
    if (comHand === "rock") {
      result = "com";
    }

    if (comHand === "paper") {
      result = "player1";
    }
  }

  return result;
};

const setActiveHand = (type, hand) => {
  document
    .querySelector(`.${type}-choice-box#${hand}`)
    .classList.add("active-hand");
};

const setInactiveHand = (type, hand) => {
  document
    .querySelector(`.${type}-choice-box#${hand}`)
    .classList.remove("active-hand");
};

export { getComHand, getResult, setActiveHand, setInactiveHand };
