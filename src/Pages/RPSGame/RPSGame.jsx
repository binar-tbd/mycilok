import React, { Component } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import {
  Title,
  // Navigation,
  ChoiceCard,
  VersusWrapper,
  RefreshButton,
  Result,
  RPSLogo,
  Back,
} from "../../components";
import { rockImage, paperImage, scissorsImage } from "../../assets/image";
import "./RPSGame.css";
import {
  getComHand,
  getResult,
  setActiveHand,
  setInactiveHand,
} from "./helper";
import { AuthContext } from "../../auth/auth";

const initialState = {
  versus: true,
  draw: false,
  player1: false,
  com: false,
  playerHand: null,
  comHand: null,
  playerScore: 0,
  comScore: 0,
  winner: null,
  userProfileId: null,
  userId: null,
};

class RPSGame extends Component {
  static contextType = AuthContext;
  state = initialState;

  userId = this.context?.currentUser?.uid;

  componentDidMount() {
    fetch(
      `http://localhost:5000/api/v1/user-profile/${this.userId}`
    )
      .then((response) => response.json())
      .then((result) => {
        // console.log(`result`, result);
        const { uuid } = result.data;
        if (result.status) {
          this.setState({ ...this.state, userProfileId: uuid });
        }
      })
      .catch((err) => {
        alert(err.message);
        this.props.history.goBack();
      });
  }

  postMatch = (winner, playerScore, comScore) => {
    /**
     * Data Dummy
     * ===========
     * player_id harusnya dari userProfileId-nya currentUser
     * game_id harusnya dari url (uuid game)
     */
    // const player_id = "617a8eb6-b078-4cb4-9eba-461a071e0515";
    // const game_id = "617a8eb6-b078-4cb4-9eba-461a071e051a";

    const requestData = {
      player_id: this.state.userProfileId,
      game_id: this.props.match.params.uuid,
      score: parseFloat(playerScore),
    };

    fetch(`http://localhost:5000/api/v1//matches`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify(requestData),
    })
      .then((response) => response.json())
      .then((result) => {
        // console.log(`result`, result);
        if (result.status)
          this.setState({
            ...this.state,
            versus: false,
            [result]: true,
            playerScore,
            comScore,
            winner,
          });
      })
      .catch((err) => console.log(err.message));
  };

  handleClick = (event) => {
    // event.preventDefault();
    let playerScore = this.state.playerScore;
    let comScore = this.state.comScore;

    if (!this.state.playerHand) {
      const playerHand = event.target.id;
      setActiveHand("player", playerHand);

      const comHand = getComHand();
      const result = getResult(playerHand, comHand);

      if (result === "player1") {
        playerScore = playerScore += 1;
      }

      if (result === "com") {
        comScore = comScore += 1;
      }

      let winner = null;
      if (playerScore === 3 || comScore === 3) {
        if (playerScore === 3) winner = "player";
        if (comScore === 3) winner = "com";
        this.postMatch(winner, playerScore, comScore);
      }

      this.setState({
        ...this.state,
        versus: false,
        [result]: true,
        playerHand,
        comHand,
        playerScore,
        comScore,
        winner,
      });
    }
  };

  handleRefresh = () => {
    // event.preventDefault();
    const { playerHand, comHand } = this.state;

    if (playerHand && comHand) {
      setInactiveHand("com", comHand);
      setInactiveHand("player", playerHand);
      this.setState({
        ...this.state,
        versus: true,
        draw: false,
        player1: false,
        com: false,
        playerHand: null,
        comHand: null,
      });
    }
  };

  handlePlay = (event) => {
    const { playerHand, comHand } = this.state;
    setInactiveHand("com", comHand);
    setInactiveHand("player", playerHand);
    this.setState(initialState);
  };

  render() {
    const {
      versus,
      draw,
      player1,
      com,
      playerScore,
      comScore,
      winner,
    } = this.state;
    return (
      <div className="rps-body">
        {/* <Navigation /> */}
        <Container fluid>
          <header className="row pt-3">
            <div className="col-auto d-flex align-items-center">
              <Back onClick={() => this.props?.history?.goBack()} />
            </div>
            <div className="col-auto d-flex align-items-center">
              <RPSLogo />
            </div>
            <div className="col-9">
              <Title label="rock paper scissors" customClass="title" />
            </div>
          </header>
        </Container>

        <main className="main-games">
          <Container fluid>
            <Row>
              <Col className="d-flex justify-content-center align-items-center text-light">
                <Title label="player 1" customClass="player-title" />
              </Col>
              <Col className="d-flex flex-column justify-content-center align-items-center">
                <h2 className="m-0 text-light">
                  {playerScore} - {comScore}
                </h2>
                {winner === "player" && (
                  <p className="m-0 bg-success py-1 px-2 text-light rounded">
                    You Win
                  </p>
                )}
                {winner === "com" && (
                  <p className="m-0 bg-danger py-1 px-2 text-light rounded">
                    You Lose
                  </p>
                )}
              </Col>
              <Col className="d-flex justify-content-center align-items-center text-light">
                <Title label="com" customClass="player-title" />
              </Col>
            </Row>

            <Row>
              <Col className="d-flex justify-content-center">
                <ChoiceCard
                  name="rock"
                  type="player-choice-box"
                  image={rockImage}
                  onClick={this.handleClick}
                />
              </Col>
              <Col></Col>
              <Col className="d-flex justify-content-center">
                <ChoiceCard
                  name="rock"
                  type="com-choice-box"
                  image={rockImage}
                />
              </Col>
            </Row>

            <Row>
              <Col className="d-flex justify-content-center">
                <ChoiceCard
                  name="paper"
                  type="player-choice-box"
                  image={paperImage}
                  onClick={this.handleClick}
                />
              </Col>
              <Col className="d-flex align-items-center justify-content-center text-center">
                {versus && <VersusWrapper />}
                {draw && <Result result="draw" label="draw" />}
                {player1 && <Result result="player-1" label="you" />}
                {com && <Result result="com" label="com" />}
              </Col>
              <Col className="d-flex justify-content-center">
                <ChoiceCard
                  name="paper"
                  type="com-choice-box"
                  image={paperImage}
                />
              </Col>
            </Row>

            <Row>
              <Col className="d-flex justify-content-center">
                <ChoiceCard
                  name="scissors"
                  type="player-choice-box"
                  image={scissorsImage}
                  onClick={this.handleClick}
                />
              </Col>
              <Col></Col>
              <Col className="d-flex justify-content-center">
                <ChoiceCard
                  name="scissors"
                  type="com-choice-box"
                  image={scissorsImage}
                />
              </Col>
            </Row>

            <Row>
              <Col></Col>
              <Col className="d-flex justify-content-center">
                {!winner && <RefreshButton onClick={this.handleRefresh} />}
                {winner && (
                  <Button
                    variant="warning text-light"
                    className="my-5"
                    size="lg"
                    onClick={this.handlePlay}
                  >
                    Play Again
                  </Button>
                )}
              </Col>
              <Col></Col>
            </Row>
          </Container>
        </main>
      </div>
    );
  }
}

export default RPSGame;
