import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./ForgotPassword.css";
import firebase from "../../services/firebase";
import { bgImage } from "../../assets/image";

class ForgotPassword extends Component {
  state = {
    email: "",
  };

  handleChangeField = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { email } = this.state;
    firebase.auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        alert("Silahkan periksa email anda untuk merubah password");
        this.props.history.push("/login");
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  render() {
    const { email } = this.state;
    return (
      <div
        className="login-body"
        style={{
          backgroundImage: `url(${bgImage})`,
          backgroundSize: "cover",
          height: "100%",
          width: "100%",
          backgroundRepeat: "no-repeat",
          backgroundAttachment: "fixed",
        }}
      >
        <div className="login-container">
          <div className="login-header">
            <h1>Form Forgot Password</h1>
          </div>
          <form onSubmit={this.handleSubmit}>
            <div id="form" class="form">
              <div className="form-inputs">
                <label htmlFor="email" className="form-label">
                  Email
                </label>
                <input
                  id="email"
                  type="email"
                  name="email"
                  className="form-input"
                  placeholder="Email"
                  value={email}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className="text-center">
                <p>
                  Don't have an account? <Link to="/register">Sign up</Link>{" "}
                </p>
              </div>
              <div className="text-center">
                <p>
                  Already Have An Account?<Link to="/login">Login</Link>{" "}
                </p>
              </div>
              <div className="text-center">
                <p>
                  Back to <Link to="/">Home</Link>{" "}
                </p>
              </div>
              <button
                type="submit"
                className="form-input-btn shadow-lg"
                onClick={this.showHandler}
              >
                Send email reset password
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default ForgotPassword;
