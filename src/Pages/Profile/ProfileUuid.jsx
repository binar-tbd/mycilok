import React, { Component, Fragment } from "react";
import { profileImage } from "../../assets/image";
import "./Profile.css";
import { Link } from "react-router-dom";
import { AuthContext } from "../../auth/auth";
import Navigation from "../../components/Navigation";
import { format } from "date-fns";
import { bgImage } from "../../assets/image";

class Profile extends Component {
  static contextType = AuthContext;
  state = {
    name: "",
    fullname: "",
    email: "",
    date: "",
    showResult: false,
    isUpdate: false,
  };

  userId = this.props.match.params.uuid;

  set = (name) => (event) => this.setState({ [name]: event.target.value });

  getUserProfile = () => {
    console.log("this.props.match", this.props.match);
    fetch(
      `http://localhost:5000/api/v1/user-profile/${this.userId}`
    )
      .then((response) => response.json())
      .then((result) => {
        // console.log(result.data);
        const { data } = result;
        if (result.status) {
          this.setState({
            ...this.state,
            name: data.full_name,
            fullname: data.full_name,
            date: format(new Date(data.birthday), "yyyy-MM-dd"),
            email: data.email,
          });
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert(err.message);
      });
  };

  componentDidMount() {
    this.getUserProfile();
    // console.log(this.props);
  }

  render() {
    const { fullname, date, email, name } = this.state;

    // console.log("this.state", this.state);
    return (
      <Fragment>
        <Navigation />
        <div
          className="profile-body"
          style={{
            backgroundImage: `url(${bgImage})`,
            backgroundSize: "cover",
            height: "100%",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundAttachment: "fixed",
          }}
        >
          <div className="wrapper">
            <div className="left">
              <img src={profileImage} alt="user" width="200" />
              <h4>{name || "username"}</h4>
            </div>
            <form onSubmit={this.submitHandle}>
              <div className="right">
                <div className="right">
                  <div className="info">
                    <h3 className="text-info">User Info</h3>
                    <div className="info_data">
                      <div className="info_data">
                        <div className="data">
                          <h4 className="text-info">Username</h4>
                          <label> {fullname}</label>
                        </div>
                        <div className="data">
                          <h4 className="text-info">Email</h4>
                          <label>{email}</label>
                        </div>
                        <div className="data">
                          <h4 className="text-info">Date of Birth</h4>
                          <label>{date}</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="main-button">
                  <Link to="/game" className="btn btn-info">
                    Back
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Profile;
