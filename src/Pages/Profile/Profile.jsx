import React, { Component, Fragment } from "react";
import { profileImage } from "../../assets/image";
import "./Profile.css";
import { Button } from "react-bootstrap";
import { AuthContext } from "../../auth/auth";
import Navigation from "../../components/Navigation";
import { format } from "date-fns";

class Profile extends Component {
  static contextType = AuthContext;
  state = {
    name: "",
    fullname: "",
    email: "",
    date: "",
    showResult: false,
    isUpdate: false,
  };

  userId = this.context.currentUser.uid;

  set = (name) => (event) => this.setState({ [name]: event.target.value });

  getUserProfile = () => {
    // console.log(`userId`, this.userId);
    fetch(
      `http://localhost:5000/api/v1/user-profile/${this.userId}`
    )
      .then((response) => response.json())
      .then((result) => {
        // console.log(result.data);
        const { data } = result;
        if (result.status) {
          this.setState({
            ...this.state,
            name: data.full_name,
            fullname: data.full_name,
            date: format(new Date(data.birthday), "yyyy-MM-dd"),
            email: data.email,
          });
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert(err.message);
      });
  };

  componentDidMount() {
    this.getUserProfile();
  }

  submitHandle = (e) => {
    const requestData = {
      full_name: this.state.fullname,
      email: this.state.email,
      birthday: this.state.date,
    };

    fetch(
      `http://localhost:5000/api/v1//user-profile/${this.userId}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify(requestData),
      }
    )
      .then((response) => response.json())
      .then((result) => {
        // console.log(`result`, result);
        if (result.status) {
          this.getUserProfile();
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert(err.message);
      });
  };

  changeHandler = (e) => {
    this.setState({
      ...this.state,
      [e.target.name]: e.target.value,
      showResult: false,
    });
  };

  showHandler = () => {
    this.setState({
      showResult: true,
    });
  };

  handleEdit = () => {
    this.setState({ ...this.state, isUpdate: !this.state.isUpdate });
  };

  render() {
    const { fullname, date, email, isUpdate, name } = this.state;
    return (
      <Fragment>
        <Navigation />
        <div className="profile-body">
          <div className="wrapper">
            <div className="left">
              <img src={profileImage} alt="user" width="200" />
              <h4>{name || "username"}</h4>
              <p>Player Profile</p>
            </div>
            <form onSubmit={this.submitHandle}>
              <div className="right">
                <div className="right">
                  <div className="info">
                    <h3>Information</h3>
                    <div className="info_data">
                      <div className="info_data">
                        <div className="data">
                          <h4>Username</h4>
                          <input
                            id="username"
                            type="text"
                            name="fullname"
                            className=""
                            placeholder="username"
                            defaultValue={fullname}
                            onChange={this.changeHandler}
                            disabled={!isUpdate}
                          />
                        </div>
                        <div className="data">
                          <h4>Email</h4>
                          <input
                            id="email"
                            type="email"
                            name="email"
                            className=""
                            placeholder="email"
                            defaultValue={email}
                            onChange={this.changeHandler}
                            disabled
                          />
                        </div>
                        <div className="data">
                          <h4>Date of Birth</h4>
                          <input
                            id="date"
                            type="date"
                            name="date"
                            className=""
                            placeholder=""
                            defaultValue={date}
                            onChange={this.changeHandler}
                            disabled={!isUpdate}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="main-button">
                  <div className="onebutton">
                    <Button
                      variant="warning"
                      disabled={!isUpdate}
                      type="submit"
                    >
                      create
                    </Button>
                  </div>
                  <div className="secondbutton">
                    <Button
                      variant="warning"
                      disabled={isUpdate}
                      onClick={this.handleEdit}
                    >
                      Edit
                    </Button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Profile;
