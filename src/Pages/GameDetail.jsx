import React, { Component } from "react";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import { gameImage, bgImage } from "../assets/image";
import Navigation from "../components/Navigation";

class GameDetail extends Component {
  state = {
    games: {},
  };

  componentDidMount() {
    const uuid = this.props.match.params.uuid;
    // console.log("uuid", this.props.match.params.uuid);
    console.log("this.props", this.props);
    fetch(`http://localhost:5000/api/v1/games/${uuid}`)
      .then((response) => response.json())
      .then((result) => {
        // console.log(result);
        this.setState({ games: result.data });
      });
  }
  render() {
    const data = this.state.games;
    const gameLink = data.name === "RPS" ? `/game/play/${data.uuid}` : "/";
    const gameClass =
      gameLink === "/"
        ? "btn btn-warning shadow-lg disabled"
        : "btn btn-warning shadow-lg";
    // console.log(data);
    return (
      <Fragment>
        <Navigation></Navigation>
        <div
          className="py-5"
          style={{
            backgroundImage: `url(${bgImage})`,
            backgroundSize: "cover",
            height: "100vh",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundAttachment: "fixed",
          }}
        >
          <div className="shadow-lg container-fluid bg-secondary rounded-3 mt-5">
            <div className="d-flex align-items-baseline">
              <div className="w-25 text-center text-white my-3 mx-2">
                <img
                  src={data.image ? data.image : gameImage}
                  width="100%"
                  alt="game"
                ></img>
                <h1>{data.name}</h1>
                <p>{data.detail}</p>
                <Link to={gameLink} className={gameClass}>
                  Play Now!
                </Link>
              </div>
              <div className="w-75 text-center my-3 mx-2">
                <h1>Leaderboard</h1>
                <div className="shadow-sm rounded-3 bg-white h-100">
                  {data.name} Leaderboard
                  <table className="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Rank</th>
                        <th scope="col">Player</th>
                        <th scope="col">Score</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    {data.name === "RPS" && (
                      <tbody>
                        {data?.leaderboards?.map((item, index) => (
                          <tr key={index}>
                            <th scope="row">{(index += 1)}</th>
                            <td>{item?.user_profile?.full_name}</td>
                            <td>{item.score}</td>
                            <td>
                              <Link
                                to={`/profile/${item?.user_profile?.user_id}`}
                                className="btn btn-info"
                              >
                                Peek
                              </Link>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    )}
                    {data.name !== "RPS" && (
                      <tbody>
                        <tr>
                          <th scope="row">1</th>
                          <td>Mark</td>
                          <td>55</td>
                          <td>
                            <Link to="#" className="btn btn-info disabled">
                              Peek
                            </Link>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">2</th>
                          <td>Jacob</td>
                          <td>43</td>
                          <td>
                            <Link to="#" className="btn btn-info disabled">
                              Peek
                            </Link>
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">3</th>
                          <td>Larry the Bird</td>
                          <td>32</td>
                          <td>
                            <Link to="#" className="btn btn-info disabled">
                              Peek
                            </Link>
                          </td>
                        </tr>
                      </tbody>
                    )}
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default GameDetail;
