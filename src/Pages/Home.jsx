import React, { Component, Fragment } from "react";
import Navigation from "../components/Navigation";
import GameJumbotron from "../components/GameJumbotron";
import { bgImage, scrollDown, gameImage } from "../assets/image";
import GameCard from "../components/GameCard";

class Home extends Component {
  state = {
    games: [],
  };
  componentDidMount() {
    fetch(`http://localhost:5000/api/v1/games/`)
      .then((response) => response.json())
      .then((result) => {
        this.setState({ games: result.data });
      });
  }
  render() {
    return (
      <Fragment>
        <Navigation />
        <div
          className="p-5"
          style={{
            backgroundImage: `url(${bgImage})`,
            backgroundSize: "cover",
            height: "270vh",
            width: "100%",
            backgroundAttachment: "fixed",
          }}
        >
          <GameJumbotron id="one" />
          <footer className="d-flex flex-column align-items-center mb-5">
            <p className="text-uppercase m-0 text-white">Choose Game</p>
            <a className="icon-scrolldown" href="#two">
              <img src={scrollDown} alt="scrolldown" />
            </a>
          </footer>
          <div className="row row mx-auto" id="two">
            {this.state.games.map((data) => {
              return (
                <GameCard
                  key={data.uuid}
                  img={data.image ? data.image : gameImage}
                  title={data.name}
                  uuid={data.uuid}
                  detail={data.detail}
                />
              );
            })}
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Home;
