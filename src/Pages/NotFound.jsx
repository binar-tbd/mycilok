import React, { Component, Fragment } from "react";
import { Container } from "reactstrap";
import Navigation from "../components/Navigation";

class NotFound extends Component {
  render() {
    return (
      <Fragment>
        <Navigation />
        <Container className="p-4">
          <h1>PAGE NOT FOUND</h1>
        </Container>
      </Fragment>
    );
  }
}

export default NotFound;
