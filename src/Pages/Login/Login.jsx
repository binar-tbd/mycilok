import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./login.css";
import firebase from "../../services/firebase";
import { bgImage } from "../../assets/image";

class Login extends Component {
  state = {
    email: "",
    password: "",
    loading: false,
  };

  handleChangeField = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ loading: true });
    const { email, password } = this.state;
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        if (res.user.emailVerified) {
          this.props.history.push("/");
        } else {
          alert("Verifikasi email anda terlebih dahulu");
          firebase.auth().signOut();
        }
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  render() {
    const { loading } = this.state;
    const { email, password } = this.state;
    return (
      <div
        className="login-body"
        style={{
          backgroundImage: `url(${bgImage})`,
          backgroundSize: "cover",
          height: "100%",
          width: "100%",
          backgroundRepeat: "no-repeat",
          backgroundAttachment: "fixed",
        }}
      >
        <div className="login-container">
          <div className="login-header">
            <h1>Login</h1>
          </div>
          <form onSubmit={this.handleSubmit}>
            <div id="form" class="form">
              <div className="form-inputs">
                <label htmlFor="email" className="form-label">
                  Email
                </label>
                <input
                  id="email"
                  type="email"
                  name="email"
                  className="form-input"
                  placeholder="Email"
                  value={email}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className="form-inputs">
                <label htmlFor="password" className="form-label">
                  Password
                </label>
                <input
                  id="password"
                  type="password"
                  name="password"
                  className="form-input"
                  placeholder="Password"
                  value={password}
                  onChange={this.handleChangeField}
                />
              </div>
              <div className="text-center">
                <p>
                  Don't have an account? <Link to="/register">Sign up</Link>{" "}
                </p>
              </div>
              <div className="text-center">
                <p>
                  Forgot password? <Link to="/forgot-password">Reset Password</Link>{" "}
                </p>
              </div>
              <div className="text-center">
                <p>
                  Back to <Link to="/">Home</Link>{" "}
                </p>
              </div>
              <button
                type="submit"
                className="form-input-btn shadow-lg"
                onClick={this.showHandler}
                disabled={loading}
              >
                {loading && <i className="spinner-border" role="status"></i>}
                {loading && <span> Processing...</span>}
                {!loading && <span>Login</span>}
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default Login;
