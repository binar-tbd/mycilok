import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "./Pages/Home";
import Game from "./Pages/Game";
import NotFound from "./Pages/NotFound";
import Register from "./Pages/Register/Register";
import Profile from "./Pages/Profile/Profile";
import ProfileUuid from "./Pages/Profile/ProfileUuid";
import Login from "./Pages/Login/Login";
import GameDetail from "./Pages/GameDetail";
import RPSGame from "./Pages/RPSGame/RPSGame";
import ForgotPassword from "./Pages/ForgotPassword/ForgotPassword";

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/game" component={Game} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/profile" component={Profile} />
          <Route exact path="/profile/:uuid" component={ProfileUuid} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/game/:uuid" component={GameDetail} />
          <Route exact path="/game/play/:uuid" component={RPSGame} />
          <Route exact path="/forgot-password" component={ForgotPassword} />       
          <Route component={NotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
